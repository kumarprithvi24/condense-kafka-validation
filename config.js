const KAFKA = {
    BROKERS: ['localhost:9092'],
    CLIENT_ID: 'condense_kafka_app',
    CONSUMER_GROUP_ID: 'condense_g1',
    TOPIC: 'condense_new_topic'
};

module.exports = { KAFKA };
